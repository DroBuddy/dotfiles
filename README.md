# README #

### What is this repository for? ###

A series of dot files for common Unix-based programs and utilities to assist the every day Sys Admin become more efficient and effective at their jobs.

* Version 0.1
* No license at this time.

### How do I get set up? ###

* Summary of set up
<pre>git clone https://bitbucket.org/DroBuddy/dotfiles</pre>
* Configuration
<pre>cp</pre> the desired dot files into your $HOME directory.
* Dependencies
Coming soon.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact