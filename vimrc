execute pathogen#infect()

syntax on
filetype indent plugin on
set modeline
set sessionoptions=options
set background=dark
